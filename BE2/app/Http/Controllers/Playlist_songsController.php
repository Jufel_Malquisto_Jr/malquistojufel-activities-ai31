<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_songs;

class Playlist_songsController extends Controller {

    public function Playlistsongs_display() {
        return DB::table('playlist_songs')->get();
    }

    //Add Songs to Playlist
    public function store(Request $request) {

        $newPlaylistSongs = new Playlist_songs();
        $newPlaylistSongs->Song_ID = $request->Song_ID;
        $newPlaylistSongs->Playlist_ID = $request->Playlist_ID;
        $newPlaylistSongs->save();
        return $newPlaylistSongs;
    }
}
