<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist_songs extends Model
{
    protected $table = 'playlist_songs';
    use HasFactory;
}
