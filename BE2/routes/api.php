<?php

use App\Http\Controllers\SongsController;
use App\Http\Controllers\PlaylistsController;
use App\Http\Controllers\Playlist_songsController;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
    
});

//Route for Songlist
Route::get('/Songs', [SongsController::class, 'Songs_display']);
Route::post('/Upload', [SongsController::class, 'store']);

//Route for Playlists
Route::get('/Playlist', [PlaylistsController::class, 'Playlists_display']);
Route::post('/Create', [PlaylistsController::class, 'store']);

//Route for PlaylistSongs
Route::get('/Playlist_songs', [Playlist_songsController::class, 'Playlistsongs_display']);
Route::post('/Create', [Playlist_songsController::class, 'store']);